//
//  Defines.h
//  Meditopia
//
//  Created by Andrei Neag on 06/01/14.
//
//

#import <Foundation/Foundation.h>

@interface Defines : NSObject

// Other constants
extern NSString *const kEmptyString;
extern NSString *const kDefaultImageString;

// Names for the main folders used for caching images
extern NSString *const kImagesCacheFolder;
extern NSString *const kLocalCacheFolder;

// The default cache time for images
extern double const kCacheTimeInSeconds;

@end
