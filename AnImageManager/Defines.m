//
//  Defines.m
//  Meditopia
//
//  Created by Andrei Neag on 06/01/14.
//
//

#import "Defines.h"

@implementation Defines

// Other constants
NSString * const kEmptyString = @"";
NSString * const kDefaultImageString = @"DefaultImageString";

// Names for the main folders used for caching images
NSString* const kImagesCacheFolder = @"Images";
NSString* const kLocalCacheFolder = @"LocalCache";


// The default cache time for images
double const kCacheTimeInSeconds = 86400;

@end
