//
//  ANImageView.m
//  Meditopia
//
//  Created by Andrei Neag on 06/01/14.
//
//
// In order for this class to work, the project needs to have AFNetwork library imported.
// To use the manager, ust call the images as you  would use the AFNetwork library.

#import "ANImageView.h"
#import "Defines.h"
#import "FileManagerHelper.h"
#import <objc/runtime.h>
#import "UIImage+Resize.h"
#import "ANImageSaverManager.h"
#import "ANNetworkController.h"
#import "ANImageHelper.h"

@interface  ANImageView()


@end

@implementation ANImageView

@synthesize placeholderImage = _placeholderImage;
@synthesize imageSaver = _imageSaver;

#pragma mark - Private Methods



#pragma mark - Overridden methods

//- (void)setImage:(UIImage *)image {
//    if (!image) {
//        image = self.placeholderImage;
//    }
//    [super setImage:image];
//}

#pragma mark - Public Methods


- (void)registerNotificationWitName:(NSString *)notificationName {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // Register for notification for image download
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(imageDownloaded:)
                                                 name:notificationName
                                               object:nil];
}

- (void)setImageWithURL:(NSURL *)url {
    [self setImageWithURL:url placeholderImage:nil];
}

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholderImage
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self setImageWithURLRequest:request placeholderImage:placeholderImage success:nil failure:nil];
}

/*
 * Asynchronously downloads an image from the specified URL, and sets it once the request is finished.
 * Any previous image request for the receiver will be cancelled.
 *
 * If the image is cached locally, the image is set immediately, otherwise a default transparent placeholder image will be set immediately,
 * and then the remote image will be set once the request is finished.
 *      @params:
 *          - url: The URL used for the image request.
 *          - imageSize: The size of the image you can choose from three defauls sizes for the image saved in cache.
 *                          	ANSavedImageOriginalSize - The original size for the image.
 *                              ANSavedImageMiddleSize - A middle sized image, used for loading in a cell or where there is no need to load the full sized image.
 *                              ANSavedImageSmallSize -Small version of the image used to show it as a thumbnail image.
 *
 */
- (void)setImageWithURL:(NSURL *)url withSavedImageSize:(ANSavedImageSize)imageSize {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
        [self setImageWithURLRequest:request
                    placeholderImage:nil
                        forImageSize:imageSize
                             success:nil
                             failure:nil];
    });
}

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholderImage
     withSavedImageSize:(ANSavedImageSize)imageSize {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
        [self setImageWithURLRequest:request
                    placeholderImage:placeholderImage
                        forImageSize:imageSize
                             success:nil
                             failure:nil];
    });
}

- (void)setImageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                  forImageSize:(ANSavedImageSize)imageSize
                       success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                       failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure {
    self.placeholderImage = placeholderImage;
    NSString * imageName = [ANImageHelper imageNameFromLinkString:[urlRequest.URL absoluteString]];
    if (0 == [imageName length] || !placeholderImage) {
         placeholderImage = [UIImage imageNamed:@"no_image.png"];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self registerNotificationWitName:imageName];

    NSString * localimagesFolder = [FileManagerHelper createLocalCachedImagesFolder];
    NSString * localPathForImage = [localimagesFolder stringByAppendingPathComponent:imageName];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    
    // See if the image did not expired in the meantime...
    NSDictionary* attrs = [fileManager attributesOfItemAtPath:localPathForImage error:nil];
    if (attrs != nil) {
        NSDate *cacheDate = (NSDate*)[attrs objectForKey: NSFileCreationDate];
        NSDate *now = [NSDate date];
        NSTimeInterval cacheTimePassed = [now timeIntervalSinceDate:cacheDate];
        if (cacheTimePassed >= kCacheTimeInSeconds) {
            [fileManager removeItemAtPath:localPathForImage error:NULL];
        }
    }
    
    if ([fileManager fileExistsAtPath:localPathForImage]) {
        UIImage * image = [UIImage imageWithContentsOfFile:localPathForImage];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:imageName
                                                                object:image];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:imageName
                                                                object:placeholderImage];
        });
        NSMutableDictionary* userData = [NSMutableDictionary dictionary];
        [userData setObject:self forKey:kImageItemKey];
        [[ANImageSaverManager sharedInstance] saveImageFromUrl:[urlRequest.URL absoluteString]
                                                        toPath:localPathForImage
                                                      withSize:CGSizeZero
                                          notificationOnFinish:imageName
                                                      userData:userData
                                                   andPriority:kMediumPriority];
    }
}

- (void)setImageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                       success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                       failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [self setImageWithURLRequest:urlRequest
                    placeholderImage:placeholderImage
                        forImageSize:ANSavedImageOriginalSize
                             success:success
                             failure:failure];
    });
}

- (void)loadImageWithImageSaver:(ANImageSaver *)imageSaver withPlaceholderImage:(UIImage*)placeholderImage {
    NSString * imageName = [ANImageHelper imageNameFromLinkString:imageSaver.imageUrl];
    imageSaver.imageDelegate = self;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self registerNotificationWitName:imageName];
    imageSaver.notificationString = imageName;
    self.placeholderImage = placeholderImage;
    if (!imageSaver.image) {
        self.image = placeholderImage;
    } else {
        self.image = imageSaver.image;
    }
    self.imageSaver = imageSaver;
    [[NSNotificationCenter defaultCenter] postNotificationName: @"kImageLoadedNotification" object: nil];
}

- (void)imageDownloaded:(NSNotification*)notif {
    id object = notif.object;
    if ([object isKindOfClass:[UIImage class]]) {
        self.image = (UIImage *)object;
    } else if ([object isKindOfClass:[ANImageSaver class]]) {
        ANImageSaver * saver = (ANImageSaver *)object;
        if (self.imageSaver && [self.imageSaver isEqual:saver]) {
           if (saver.image) {
               [self loadImageAnimated:saver.image];
           }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"kImageDownloadedNotification" object: nil];
    } else {
        self.image = nil;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadImageAnimated:(UIImage *)image {
    UIImageView *animationImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    animationImageView.image = image;
    animationImageView.alpha = 0.3f;
    animationImageView.contentMode = self.contentMode;
    animationImageView.layer.cornerRadius = self.layer.cornerRadius;
    animationImageView.backgroundColor = self.backgroundColor;
    animationImageView.tintColor = self.tintColor;
    float animationTime = 0.15;
    [self addSubview:animationImageView];
    [UIView animateWithDuration:animationTime
                     animations:^{
                         animationImageView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         self.image = image;
                         animationImageView.alpha = 0;
                         [animationImageView removeFromSuperview];
                     }];
}

#pragma mark - Image saver protocol methods

- (void)imageDownloadedForImageSaver:(ANImageSaver *)imageSaver {
    if ([imageSaver isKindOfClass:[ANImageSaver class]]) {
        if (self.imageSaver && [self.imageSaver isEqual:imageSaver]) {
            if (imageSaver.image) {
                [self loadImageAnimated:imageSaver.image];
            }
        }
    } else {
        self.image = nil;
    }
}

@end

