//
//  ANImageSaverManager.h
//
//  Created by Andrei Neag on 4/20/11.
//  Copyright 2011. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ANImageSaver.h"

#define kURLKey            @"url"
#define kPathKey           @"path"
#define kImageSizeKey      @"size"
#define kImageHeightKey    @"height"
#define kImageWidthKey     @"width"
#define kNotificationKey   @"notification"
#define kImageItemKey      @"imageItemKey"
#define kUserInfoKey       @"userInfo"
#define kPriorityKey       @"priority"
#define kTagKey            @"tag"

// List browser carousel objects

enum kPrioritiesList {
   kLowestPriority = 0,
   kLowPriority,
   kMediumPriority,
   kHighPriority,
   kGlobalPriority /* connections that need to run in background */
}kPrioritiesList;

@interface ANImageSaverManager : NSObject <ANImageSaverDelegate> {
   
   BOOL              _sendNotification; // Flag for avoiding sending too many notifications to tableviews.
   
   NSMutableArray       *_pendingConnections;
   NSInteger            _runningConnectionsCount;
   // addign lists for pending connections is needed more lists in order to make prioritized requests
   NSMutableArray       *_highPendingConnections;// top priority
   NSMutableArray       *_mediumPendingConnections;
   NSMutableArray       *_lowPendingConnections;
   NSMutableArray       *_lowestPendingConnections;
   NSMutableDictionary  *_activeRunningConnections;
   NSMutableArray       *_globalPendingConnections;
   NSMutableDictionary  *_globalRunningConnections;
   
   // for serial downloading of posters and backdrops
   NSMutableArray       *_arrayOfSerialPosters;
   NSMutableArray       *_arrayOfSerialHomePosters;
   NSMutableArray       *_arrayOfSerialTVHomePosters;
   NSMutableArray       *_arrayOfSerialBackdrops;
   NSMutableArray       *_arrayOfSerialMiniPosters;
   NSMutableArray       *_arrayOfSerialTVMiniPosters;
   
   // notification to be thrown after the first poster is downloaded
   NSString             *_firstPosterImageDownloadedNotification;
   BOOL                 _firstPosterWasDownloaded;
   BOOL                 _backdropsRetrievalPaused;
   BOOL                 _postersRetrievalPaused;
}

// for serial downloading of posters and backdrops
@property (nonatomic, strong) NSMutableArray       *arrayOfSerialPosters;
@property (nonatomic, strong) NSMutableArray       *arrayOfSerialBackdrops;
@property (nonatomic, strong) NSMutableArray       *arrayOfSerialHomePosters;
@property (nonatomic, strong) NSMutableArray       *arrayOfSerialTVHomePosters;
@property (nonatomic, strong) NSMutableArray       *arrayOfSerialMiniPosters;
@property (nonatomic, strong) NSMutableArray       *arrayOfSerialTVMiniPosters;
// notification to be thrown after the first poster is downloaded
@property (nonatomic, strong) NSString             *firstPosterImageDownloadedNotification;
@property (nonatomic, assign) NSInteger runningConnectionsCount;
@property (nonatomic, strong) NSMutableArray       *pendingConnections;
// addign lists for pending connections is needed more lists in order to make prioritized requests
@property (nonatomic, strong) NSMutableArray       *highPendingConnections;// top priority
@property (nonatomic, strong) NSMutableArray       *mediumPendingConnections;
@property (nonatomic, strong) NSMutableArray       *lowPendingConnections;
@property (nonatomic, strong) NSMutableArray       *lowestPendingConnections;
@property (nonatomic, strong) NSMutableDictionary  *activeRunningConnections;
@property (nonatomic, strong) NSMutableArray       *globalPendingConnections;
@property (nonatomic, strong) NSMutableDictionary  *globalRunningConnections;


// singelton method
+ (id)sharedInstance;

// public methods
- (void)saveImageFromUrl:(NSString *)urlString
                  toPath:(NSString *)savePath 
                withSize:(CGSize)imageSize 
    notificationOnFinish:(NSString *)notificationOnFinish 
                userData:(NSDictionary *)userData
             andPriority:(int)priority;

- (void)saveImageFromUrl:(NSString *)urlString
                  toPath:(NSString *)savePath 
                withSize:(CGSize)imageSize 
    notificationOnFinish:(NSString *)notificationOnFinish 
                userData:(NSDictionary *)userData
             andPriority:(int)priority
                 withTag:(NSUInteger)tag;

- (void)imageDownloadedNotification:(NSNotification *)notification;
- (void)postSaveImagesNotification:(ANImageSaver *)imageSaver;

- (void)startNextPendingConnectionFromArray:(NSMutableArray *)notificationArray;

- (void)removePendingRequestsWithPriority:(int)priority;
- (void)removeAllConnections;
- (void)removeRunningConnections;

// for serial downloading of posters and backdrops
- (void)cancelAllBackdropsRequests;
- (void)cancelAllPostersRequests;

// serial downloading
- (void)startDownloadingBackdropsFromArray:(NSArray *)backdropsArray;
- (void)pauseDownloadingBackdrops;
- (void)continueDownloadingBackdrops;

- (void)startDownloadingHomePostersFromArray:(NSArray *)backdropsArray;
- (void)startDownloadingHomeMiniPostersFromArray:(NSArray *)backdropsArray;
- (void)startDownloadingTVHomeMiniPostersFromArray:(NSArray *)backdropsArray;
- (void)startDownloadingTVHomePostersFromArray:(NSArray *)backdropsArray;

- (void)startDownloadingPostersFromArray:(NSArray *)postersArray;
- (void)pauseDownloadingPosters;
- (void)continueDownloadingPosters;

@end
