//
//  ANImageView.h
//  Meditopia
//
//  Created by Andrei Neag on 06/01/14.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Availability.h>
#import <UIKit/UIKit.h>
#import "ANImageSaver.h"

typedef enum {
	ANSavedImageOriginalSize = 0,
	ANSavedImageMiddleSize,
    ANSavedImageSmallSize
} ANSavedImageSize;

/**
 This category adds methods to the UIKit framework's `UIImageView` class. The methods in this category provide support for loading remote images asynchronously from a URL.
 */
@interface ANImageView: UIImageView <ANImageSaverDelegate>{
    UIImage * _placeholderImage;
}

@property (nonatomic, strong) UIImage * placeholderImage;
@property (nonatomic, strong) ANImageSaver * imageSaver;
///--------------------
/// @name Setting Image
///--------------------

/**
 Asynchronously downloads an image from the specified URL, and sets it once the request is finished. Any previous image request for the receiver will be cancelled.
 
 If the image is cached locally, the image is set immediately, otherwise the specified placeholder image will be set immediately, and then the remote image will be set once the request is finished.
 
 By default, URL requests have a cache policy of `NSURLCacheStorageAllowed` and a timeout interval of 30 seconds, and are set not handle cookies. To configure URL requests differently, use `setImageWithURLRequest:placeholderImage:success:failure:`
 
 @param url The URL used for the image request.
 */
- (void)setImageWithURL:(NSURL *)url;

/*
 * Asynchronously downloads an image from the specified URL, and sets it once the request is finished.
 * Any previous image request for the receiver will be cancelled.
 *
 * If the image is cached locally, the image is set immediately, otherwise a default transparent placeholder image will be set immediately, 
 * and then the remote image will be set once the request is finished.
 *      @params: 
 *          - url: The URL used for the image request.
 *          - imageSize: The size of the image you can choose from three defauls sizes for the image saved in cache.
 *                          	ANSavedImageOriginalSize - The original size for the image.
 *                              ANSavedImageMiddleSize - A middle sized image, used for loading in a cell or where there is no need to load the full sized image.
 *                              ANSavedImageSmallSize -Small version of the image used to show it as a thumbnail image.
 *
 */

- (void)setImageWithURL:(NSURL *)url withSavedImageSize:(ANSavedImageSize)imageSize;
- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholderImage
     withSavedImageSize:(ANSavedImageSize)imageSize;


/*
 * Asynchronously downloads an image from the specified URL, and sets it once the request is finished.
 * Any previous image request for the receiver will be cancelled.
 *
 * If the image is cached locally, the image is set immediately, otherwise a default transparent placeholder image will be set immediately,
 * and then the remote image will be set once the request is finished.
 *      @params:
 *          - url: The URL used for the image request.
 *          - imageSize: The size of the image you can choose from three defauls sizes for the image saved in cache.
 *                          	ANSavedImageOriginalSize - The original size for the image.
 *                              ANSavedImageMiddleSize - A middle sized image, used for loading in a cell or where there is no need to load the full sized image.
 *                              ANSavedImageSmallSize -Small version of the image used to show it as a thumbnail image.
 *          - placeholderImage: the image to be used until the correct image is being downloaded. 
 *                              If it is nil, then a default placeholder with a tranparent image is used.
 *          - success/failure: the blocks oof code used for success and failure of downloading images.
 *
 */

- (void)setImageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                  forImageSize:(ANSavedImageSize)imageSize
                       success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                       failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;

/**
 Asynchronously downloads an image from the specified URL, and sets it once the request is finished. Any previous image request for the receiver will be cancelled.
 
 If the image is cached locally, the image is set immediately, otherwise the specified placeholder image will be set immediately, and then the remote image will be set once the request is finished.
 
 By default, URL requests have a cache policy of `NSURLCacheStorageAllowed` and a timeout interval of 30 seconds, and are set not handle cookies. To configure URL requests differently, use `setImageWithURLRequest:placeholderImage:success:failure:`
 
 @param url The URL used for the image request.
 @param placeholderImage The image to be set initially, until the image request finishes. If `nil`, the image view will not change its image until the image request finishes.
 */
- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholderImage;

/**
 Asynchronously downloads an image from the specified URL request, and sets it once the request is finished. Any previous image request for the receiver will be cancelled.
 
 If the image is cached locally, the image is set immediately, otherwise the specified placeholder image will be set immediately, and then the remote image will be set once the request is finished.
 
 If a success block is specified, it is the responsibility of the block to set the image of the image view before returning. If no success block is specified, the default behavior of setting the image with `self.image = image` is applied.
 
 @param urlRequest The URL request used for the image request.
 @param placeholderImage The image to be set initially, until the image request finishes. If `nil`, the image view will not change its image until the image request finishes.
 @param success A block to be executed when the image request operation finishes successfully. This block has no return value and takes three arguments: the request sent from the client, the response received from the server, and the image created from the response data of request. If the image was returned from cache, the request and response parameters will be `nil`.
 @param failure A block object to be executed when the image request operation finishes unsuccessfully, or that finishes successfully. This block has no return value and takes three arguments: the request sent from the client, the response received from the server, and the error object describing the network or parsing error that occurred.
 */
- (void)setImageWithURLRequest:(NSURLRequest *)urlRequest
              placeholderImage:(UIImage *)placeholderImage
                       success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                       failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;

- (void)loadImageWithImageSaver:(ANImageSaver *)imageSaver withPlaceholderImage:(UIImage*)placeholderImage;

@end
