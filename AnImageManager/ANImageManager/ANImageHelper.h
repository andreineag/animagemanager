//
//  ANImageHelper.h
//  Meditopia
//
//  Created by Andrei Neag on 05/02/14.
//
//

#import <Foundation/Foundation.h>

@interface ANImageHelper : NSObject {
    
}

+ (NSString *)imageNameFromLinkString:(NSString *)linkString;

/*
 Method used to perform a selector to avoid a warning.
 Parameters:
 - selector: the selector that will be called;
 - requestor: the caller object that made the request in the first place;
 */
+ (void)performSelector:(SEL)selector forRequestor:(id<NSObject>)requestor;

@end
