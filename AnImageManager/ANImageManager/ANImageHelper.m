//
//  ANImageHelper.m
//  Meditopia
//
//  Created by Andrei Neag on 05/02/14.
//
//

#import "ANImageHelper.h"
#import "ANImageSaver.h"
#import "Defines.h"

@implementation ANImageHelper


+ (NSString *)imageNameFromLinkString:(NSString *)linkString {
    if (0 == [linkString length]) {
        return kDefaultImageString;
    }
  
    //Shailee 02-19: First of all server shouldn't return void urls for images also we should check the correct condition.
    
    NSArray * pathComponents = [linkString pathComponents];
    int pathArrayCount = [pathComponents count];
    if (pathArrayCount-2 < 0) {
        return  linkString;
    }
    
    NSString * lastPathComponent = [pathComponents lastObject];
    NSString * secondLastPathComponent = [pathComponents objectAtIndex:[pathComponents count] - 2];

    NSString * thirdPahComponent = nil;
    if (3 < [pathComponents count]) {
        thirdPahComponent = [pathComponents objectAtIndex:[pathComponents count] - 3];
    }
    
    NSString * imageName = [NSString stringWithFormat:@"%@_%@",secondLastPathComponent, lastPathComponent];
    if (0 < [thirdPahComponent length]) {
        imageName = [NSString stringWithFormat:@"%@_%@", thirdPahComponent, imageName];
    }
    return imageName;
}

/*
 Method used to perform a selector to avoid a warning.
 Parameters:
 - selector: the selector that will be called;
 - requestor: the caller object that made the request in the first place;
 */
+ (void)performSelector:(SEL)selector forRequestor:(id<NSObject>)requestor {
    if ([requestor respondsToSelector:selector]) {
        if ([requestor isKindOfClass:[ANImageSaver class]]) {
            ANImageSaver * controller = (ANImageSaver *)requestor;
            IMP imp = [controller methodForSelector:selector];
            void (*func)(id, SEL) = (void *)imp;
            func(controller, selector);
        }
    }
}

@end
