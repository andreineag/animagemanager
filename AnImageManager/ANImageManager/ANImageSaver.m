//
//  ANImageSaver.m
//
//  Created by Andrei Neag on 02/23/11.
//  Copyright 2011. All rights reserved.
//
 
#import "ANImageSaver.h"
#import "ANNetworkController.h"
#import "UIImage+Resize.h"
#import "Defines.h"

@interface ANImageSaver (Private)

- (void)downloadFinished:(id)sender;
- (void)notifyImageIsDownloaded;

@end

@implementation ANImageSaver

#pragma mark Synthesize calls
@synthesize pathToSaveTheImage = _pathToSaveTheImage;
@synthesize numberOfRetries = _numberOfRetries;
@synthesize wasSuccess = _wasSuccess;
@synthesize notificationString = _notificationString;
@synthesize imageUrl = _imageUrl;
@synthesize imageSize = _imageSize;
@synthesize controller = _controller;
@synthesize userData = _userData;
@synthesize tag = _tag;
@synthesize image = _image;
@synthesize imageDelegate;

NSString * const kUmageUrlKey = @"saveImageUrl";
NSString * const kUmagePathUrl = @"saveImagePath";

#pragma mark -
#pragma mark Implementation

- (id)initWithUrl:(NSString *)imageUrl withPath:(NSString *)path imageSize:(CGSize)imageSize andDelegate:(id<ANImageSaverDelegate>) delegate {
   if (self = [super init]) {
      self.imageSize = imageSize;
      self.pathToSaveTheImage = path;
      self.imageDelegate = delegate;
      self.imageUrl = imageUrl;

       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
           NSFileManager * fileManager = [NSFileManager defaultManager];
           // See if the image did not expired in the meantime...
           NSDictionary* attrs = [fileManager attributesOfItemAtPath:self.pathToSaveTheImage error:nil];
           if (attrs != nil) {
               NSDate *cacheDate = (NSDate*)[attrs objectForKey: NSFileCreationDate];
               NSDate *now = [NSDate date];
               NSTimeInterval cacheTimePassed = [now timeIntervalSinceDate:cacheDate];
               if (cacheTimePassed >= kCacheTimeInSeconds) {
                   [fileManager removeItemAtPath:self.pathToSaveTheImage error:NULL];
               }
           } 
           
           if(![fileManager fileExistsAtPath:path] && imageUrl && 0 < [imageUrl length]) {
               dispatch_async(dispatch_get_main_queue(), ^{
                   self.controller = [[ANNetworkController alloc] initWithUrl:imageUrl target:self actionOnFinish:@selector(downloadFinished:)];
               });
           } else {
               _wasSuccess = (imageUrl && 0 < [imageUrl length]);
               self.image = [UIImage imageWithContentsOfFile:self.pathToSaveTheImage];
               dispatch_after(0, dispatch_get_main_queue(), ^{
                   [self notifyImageIsDownloaded];
               });
           }
       });
   }
    
   return self;
}

- (id)initWithUrl:(NSString *)imageUrl withPath:(NSString *)path imageSize:(CGSize)imageSize {
    self = [self initWithUrl:imageUrl withPath:path imageSize:imageSize andDelegate:nil];
    return self;
}

- (id)initWithUrl:(NSString *)imageUrl withPath:(NSString *)path imageSize:(CGSize)imageSize delegate:(id<ANImageSaverDelegate>) delegate andUserData:(NSDictionary *)userData {
   if (self = [self initWithUrl:imageUrl withPath:path imageSize:imageSize andDelegate:delegate]) {
      self.userData = userData;
   }
	return self;
}

- (void)startDownload {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [self.controller.connection start];
    });
}

- (void)dealloc {
    [self.controller.connection cancel];
    self.controller.connection = nil;
    self.controller.target = nil;
   if(nil == _notificationString) {
//      NSLog(@"Image saver previously released: %d with tag = %d", (int)self, _tag);
   }
    self.controller.target = nil; // unregister
    self.controller = nil;
    self.pathToSaveTheImage = nil;
    self.notificationString = nil;
    self.imageUrl = nil;
    self.userData = nil;
}

#pragma mark -
#pragma mark FHNetworkController delegate methods

- (void)downloadFinished:(id)sender {
   ANNetworkController * controller = sender;
   if(controller.wasSuccess) {
      _wasSuccess = YES;
      NSData * data = [controller getData];
      // use GCD for writing the files to disk
      NSString * path = self.pathToSaveTheImage;
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
         if (path) {
            NSError * error = nil;
            BOOL success = NO;
            if ((0 < self.imageSize.width) && (0 < self.imageSize.height)) {
               UIImage *savedImage = [UIImage imageWithData:data];
               // is a image is larger than what we need we resize it but also keep the original one
               UIImage *thumbnailImage = (savedImage.size.width > self.imageSize.width || savedImage.size.height > self.imageSize.height) ? 
                                                      [savedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit 
                                                                                       bounds:self.imageSize 
                                                                         interpolationQuality:kCGInterpolationHigh] : savedImage;
               NSData *imageData = UIImagePNGRepresentation(thumbnailImage);
               
               success = [imageData writeToFile:path options:NSAtomicWrite error:&error];
            } else {
               success = [data writeToFile:path options:NSAtomicWrite error:&error];
            }                  
            if (!success) {
        //       NSLog(@"FHImageSaver - Image could not be saved at path: %@, because: %@", path, [error localizedDescription]);
            }
         }
         
         // try to load the image in an UIImage to see if it was correctly downloaded
         UIImage * img = [UIImage imageWithContentsOfFile:path];
         self.wasSuccess = (nil == img) ? NO : YES;
          self.image = img;
         // send notification that the image is downloaded
         dispatch_async(dispatch_get_main_queue(), ^{ /* will post the notification from the main thread */
            [self notifyImageIsDownloaded];
         });
      });
   } else { // error
  //    NSLog(@"FHImageSaver error: %@ for image url: %@ ", [controller.error localizedDescription], _imageUrl);
      _wasSuccess = NO;
      // send notification that the image is downloaded
      dispatch_async(dispatch_get_main_queue(), ^{ /* will post the notification from the main thread */
         [self notifyImageIsDownloaded];
      });
   }
}

- (void)notifyImageIsDownloaded {
   if (self.imageDelegate && [self.imageDelegate respondsToSelector:@selector(imageDownloadedForImageSaver:)]) {
       [self.imageDelegate imageDownloadedForImageSaver:self];
   }
}

#pragma mark - ImageSaver delegate methods

- (void)imageDownloadedFromUrlString:(NSString *)urlString {
    [self notifyImageIsDownloaded];
}

@end
