//
//  ANImageSaverManager.m
//
//  Created by Andrei Neag on 4/20/11.
//  Copyright 2011. All rights reserved.
//

#import "ANImageSaverManager.h"
#import "ANImageSaver.h"
#import "ANImageView.h"
#import "Defines.h"

#define kMaxRunningConnections 20


#define kImageSavedNotification @"com.TPG.imageSavedNotification"

#define kBackdropSavedNotification @"com.TPG.backdropSavedNotification"
#define kHomePostersSavedNotification @"com.TPG.homePostersSavedNotification"
#define kHomeMiniPostersSavedNotification @"com.TPG.homeMiniPostersSavedNotification"
#define kTVHomeMiniPostersSavedNotification @"com.TPG.TVHomeMiniPostersSavedNotification"
#define kTVHomePostersSavedNotification @"com.TPG.homeTVPostersSavedNotification"
#define kPosterSavedNotification @"com.TPG.posterSavedNotification"

@interface ANImageSaverManager (Private)

/*
 Method used to start a new request for the first image in the imagesArray parameter.
 Parameters:
         - imagesArray : the array of images to be downloaded. The method will create a FHImage saver object for the first element in this array.
                         If this array is empty, no image saver is created
         - notificationOnFinish : a NSString representing the notification triggered after the download of the image.
 */
- (void)saveNextImageFromArray:(NSMutableArray *) imagesArray withNotificationOnFinish:(NSString *)notificationOnFinish withTag:(NSUInteger)tag;

@end

@implementation ANImageSaverManager

static ANImageSaverManager *sharedImageSaverManager = nil;

@synthesize arrayOfSerialPosters       = _arrayOfSerialPosters;
@synthesize arrayOfSerialBackdrops     = _arrayOfSerialBackdrops;
@synthesize arrayOfSerialHomePosters   = _arrayOfSerialHomePosters;
@synthesize arrayOfSerialTVHomePosters = _arrayOfSerialTVHomePosters;
@synthesize arrayOfSerialMiniPosters   = _arrayOfSerialMiniPosters;
@synthesize arrayOfSerialTVMiniPosters = _arrayOfSerialTVMiniPosters;
@synthesize firstPosterImageDownloadedNotification = _firstPosterImageDownloadedNotification;
@synthesize runningConnectionsCount = _runningConnectionsCount;
@synthesize highPendingConnections = _highPendingConnections;
@synthesize mediumPendingConnections = _mediumPendingConnections;
@synthesize lowPendingConnections = _lowPendingConnections;
@synthesize lowestPendingConnections = _lowestPendingConnections;
@synthesize activeRunningConnections = _activeRunningConnections;
@synthesize globalPendingConnections = _globalPendingConnections;
@synthesize globalRunningConnections = _globalRunningConnections;
@synthesize pendingConnections = _pendingConnections;

#pragma mark -
#pragma mark Singleton implementation

+ (id)sharedInstance {
   if (sharedImageSaverManager == nil) {
      sharedImageSaverManager = [[self alloc] init];
   }
   
   return sharedImageSaverManager;
}

+ (id)allocWithZone:(NSZone *)zone {
   if (sharedImageSaverManager == nil) {
      sharedImageSaverManager = [super allocWithZone: zone];
   }
   
   return sharedImageSaverManager;
}

- (id)init {
   self = [super init];
   if (!self) {
      return nil;
   }
   
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(imageDownloadedNotification:)
                                                name:kImageSavedNotification
                                              object:nil];
   
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(downloadNextBackdropAfterDelay:)
                                                name:kBackdropSavedNotification
                                              object:nil];
   
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(downloadNextPosterAfterDelay:)
                                                name:kPosterSavedNotification
                                              object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(downloadNextHomePosterAfterDelay:)
                                                name:kHomePostersSavedNotification 
                                              object:nil];
   
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(downloadNextTVHomePosterAfterDelay:) 
                                                name:kTVHomePostersSavedNotification 
                                              object:nil];
   
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(downloadNextMiniHomePosterAfterDelay:) 
                                                name:kHomeMiniPostersSavedNotification 
                                              object:nil];
   
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(downloadNextTVMiniHomePosterAfterDelay:)
                                                name:kTVHomeMiniPostersSavedNotification 
                                              object:nil];

   
   // Init flag 
   _sendNotification = YES;
   
   // initiating the arrays for connections
   self.pendingConnections         = [[NSMutableArray alloc] init];
   self.highPendingConnections     = [[NSMutableArray alloc] init];
   self.mediumPendingConnections   = [[NSMutableArray alloc] init];
   self.lowPendingConnections      = [[NSMutableArray alloc] init];
   self.lowestPendingConnections   = [[NSMutableArray alloc] init];
   self.activeRunningConnections   = [[NSMutableDictionary alloc] init];
   self.globalPendingConnections   = [[NSMutableArray alloc] init];
   self.globalRunningConnections   = [[NSMutableDictionary alloc] init];
   self.runningConnectionsCount = 0;
   
   // for serial downloading of posters and backdrops
   self.arrayOfSerialPosters       = [[NSMutableArray alloc]init];
   self.arrayOfSerialBackdrops     = [[NSMutableArray alloc] init];
   _backdropsRetrievalPaused = NO;
   _postersRetrievalPaused = NO;
   
	return self;
}

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
   [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark -
#pragma mark Public Methods


/*
 * Method used to start downloading a new image.
 */
- (void)startDownloading:(NSDictionary *)params {
   NSString *urlString              = [params objectForKey:kURLKey];
   int tag                          = [[params objectForKey:kTagKey] intValue];
   NSString *savePath               = [params objectForKey:kPathKey];
   NSString *notificationOnFinish   = [params objectForKey:kNotificationKey];
   float    imageHeight             = [[params objectForKey:kImageHeightKey] floatValue];
   float    imageWidth              = [[params objectForKey:kImageWidthKey] floatValue];
   NSDictionary * userData          = [params objectForKey:kUserInfoKey];
   int priority                     = [[params objectForKey:kPriorityKey] intValue];
   
   NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:userData];
   [dict setObject:notificationOnFinish forKey:kNotificationKey];
    ANImageSaver * saver = [[ANImageSaver alloc] initWithUrl:urlString
                                                    withPath:savePath
                                                   imageSize:CGSizeMake(imageWidth, imageHeight)
                                                    delegate:self
                                                 andUserData:dict];
   saver.tag = tag;
   NSString * key = [NSString stringWithFormat:@"%d", (int)saver];
   [_activeRunningConnections setObject:saver forKey:key];
   if (kGlobalPriority == priority) { // if this is a global priority request add it also to the global set
      [_globalRunningConnections setObject:saver forKey:key];
   }
}

- (void)saveImageFromUrl:(NSString *)urlString
                  toPath:(NSString *)savePath 
                withSize:(CGSize)imageSize 
    notificationOnFinish:(NSString *)notificationOnFinish 
                userData:(NSDictionary *)userData
             andPriority:(int)priority {
   [self saveImageFromUrl:urlString toPath:savePath withSize:imageSize notificationOnFinish:notificationOnFinish userData:userData andPriority:priority withTag:0];
   
}

- (void)saveImageFromUrl:(NSString *)urlString 
                  toPath:(NSString *)savePath 
                withSize:(CGSize)imageSize 
    notificationOnFinish:(NSString *)notificationOnFinish 
                userData:(NSDictionary *)userData
             andPriority:(int)priority
                 withTag:(NSUInteger)tag {
   
   if(nil == urlString || nil == savePath || nil == notificationOnFinish) {
      NSLog(@"ANImageSaverManager: Invalid arguments received .. skiping image");
      
      dispatch_async(dispatch_get_main_queue(), ^{
         [[NSNotificationCenter defaultCenter] postNotificationName:notificationOnFinish
                                                             object:nil];
      });
      return;
   }
   if(self.runningConnectionsCount < kMaxRunningConnections) {

      NSMutableDictionary * dict = [NSMutableDictionary dictionaryWithDictionary:userData];
       ANImageView* senderImage = [dict objectForKey:kImageItemKey];
       if (senderImage) {
           [dict removeObjectForKey:kImageItemKey];
       }
      [dict setObject:notificationOnFinish forKey:kNotificationKey];
      dispatch_async(dispatch_get_main_queue(), ^{
         ANImageSaver * saver = [[ANImageSaver alloc] initWithUrl:urlString
                                                         withPath:savePath
                                                        imageSize:imageSize
                                                         delegate:self
                                                      andUserData:dict];
          if (senderImage) {
              senderImage.imageSaver = saver;
          }

          ANImageView* senderImage = [dict objectForKey:kImageItemKey];
          if (senderImage) {
              senderImage.imageSaver = saver;
              [dict removeObjectForKey:kImageItemKey];
          }
         saver.tag = tag;
         NSString * key = [NSString stringWithFormat:@"%d", (int)saver];
         [_activeRunningConnections setObject:saver forKey:key];
         if (kGlobalPriority == priority) { // if this is a global priority request add it also to the global set
            [_globalRunningConnections setObject:saver forKey:key];
         }
         self.runningConnectionsCount++;
      });
   } else {
      NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:urlString, kURLKey,
                                                                        [NSNumber numberWithInt:(int)tag], kTagKey,
                                                                        [NSNumber numberWithInt:priority], kPriorityKey,
                                                                        savePath, kPathKey,
                                                                        [NSNumber numberWithFloat:imageSize.width], kImageWidthKey,
                                                                        [NSNumber numberWithFloat:imageSize.height],kImageHeightKey,
                                                                        notificationOnFinish, kNotificationKey,
                                                                        userData, kUserInfoKey,
                                                                        nil];
      
      dispatch_async(dispatch_get_main_queue(), ^{
         switch (priority) {
            case kGlobalPriority: {
               [_globalPendingConnections addObject:params];
               break;
            }
            case kHighPriority:{
               [_highPendingConnections addObject:params];
               break;
            }
            case kLowPriority:{
               [_lowPendingConnections addObject:params];
               break;
            }
            default:{
               [_mediumPendingConnections addObject:params];
               break;
            }
         }
      });
   }
}

/*
 * Post notification if there isn't another timer started.
 */
- (void)imageDownloadedNotification:(NSNotification *)notification {
   self.runningConnectionsCount--;
   if (0 > self.runningConnectionsCount) {
      self.runningConnectionsCount = 0;
   }
   
   if (0 < [_globalPendingConnections count]) {
      [self startNextPendingConnectionFromArray:_globalPendingConnections];
   } else if (0 < [_highPendingConnections count]) {
      [self startNextPendingConnectionFromArray:_highPendingConnections];
   } else if (0 < [_mediumPendingConnections count]){
      [self startNextPendingConnectionFromArray:_mediumPendingConnections];
   } else if (0 < [_lowPendingConnections count]) {
      [self startNextPendingConnectionFromArray:_lowPendingConnections];
   } else if (0 < [_lowestPendingConnections count]) {
      [self startNextPendingConnectionFromArray:_lowestPendingConnections];
   }
   
   ANImageSaver *saver = notification.object;
   
   [self postSaveImagesNotification:saver];
   _sendNotification = NO;

   NSString * key = [NSString stringWithFormat:@"%d", (int)saver];
   [_activeRunningConnections removeObjectForKey:key];
   [_globalRunningConnections removeObjectForKey:key]; // it does nothing if it doesn't contain it
}

- (void)startNextPendingConnectionFromArray:(NSMutableArray *)notificationArray{
   dispatch_async(dispatch_get_main_queue(), ^{
      if (0 < [notificationArray count]) {
         NSDictionary *params = [notificationArray objectAtIndex:0];
            [self startDownloading:params];
            self.runningConnectionsCount++;
         
         [notificationArray removeObjectAtIndex:0];
      }
   });
}

/*
 * Post notification to update views with the new saved pictures.
 */
- (void)postSaveImagesNotification:(ANImageSaver *)imageSaver {
   // retrieve the original notification to be posted
   NSString * originalNotification = [imageSaver.userData objectForKey:kNotificationKey];
   
   // post a notification with the original notification received from caller   
   if(originalNotification) {
       if (!imageSaver.controller) {
           [[NSNotificationCenter defaultCenter] postNotificationName:originalNotification
                                                               object:imageSaver.image];
       } else {
           [[NSNotificationCenter defaultCenter] postNotificationName:originalNotification object:imageSaver];
       }
   }
}

#pragma mark -
#pragma mark pending requests remover

- (void)removePendingRequestsWithPriority:(int)priority{
   //NSLog(@"FHImageSaverManager - number of low priority requests is: %d", [_lowPendingConnections count]);
   switch (priority) {
      case kGlobalPriority: {
         [_globalRunningConnections removeAllObjects];
         [_globalPendingConnections removeAllObjects];
         break;
      }
      case kHighPriority:{
         [_highPendingConnections removeAllObjects];
         break;
      }
      case kMediumPriority:{
         [_highPendingConnections removeAllObjects];
         [_mediumPendingConnections removeAllObjects];
         break;
      }
      case kLowPriority:{
         [_highPendingConnections removeAllObjects];
         [_mediumPendingConnections removeAllObjects];
         [_lowPendingConnections removeAllObjects];
         break;
      }
      case kLowestPriority:
         [_highPendingConnections removeAllObjects];
         [_mediumPendingConnections removeAllObjects];
         [_lowPendingConnections removeAllObjects];
         [_lowestPendingConnections removeAllObjects];
         break;

      default:
         break;
   }
}

- (void)removeAllConnections {
   dispatch_async(dispatch_get_main_queue(), ^{
      [_highPendingConnections removeAllObjects];
      [_mediumPendingConnections removeAllObjects];
      [_lowPendingConnections removeAllObjects];
      [self cancelAllPostersRequests];
      [self cancelAllBackdropsRequests];
      [self removeRunningConnections];
   });
}
   
- (void)removeRunningConnections {
   [_activeRunningConnections removeAllObjects];
   self.runningConnectionsCount = [_globalRunningConnections count];
   
   // Start the next global image download.
   if (0 == self.runningConnectionsCount && 0 < [_globalPendingConnections count]) {
      [self startNextPendingConnectionFromArray:_globalPendingConnections];
   }
}

#pragma mark -
#pragma mark Serial download for bacdrops and posters

// Home Home TV posters download
- (void)startDownloadingTVHomePostersFromArray:(NSArray *)backdropsArray {
   self.arrayOfSerialTVHomePosters = [NSMutableArray arrayWithArray:backdropsArray];
   
   if (1 < [backdropsArray count]) {
      [self saveNextImageFromArray:self.arrayOfSerialTVHomePosters 
          withNotificationOnFinish:kTVHomePostersSavedNotification 
                           withTag:(NSUInteger)1111];
   }
}

// Start downloading posters from  Home Home movie
- (void)startDownloadingHomeMiniPostersFromArray:(NSArray *)backdropsArray {
   self.arrayOfSerialMiniPosters = [NSMutableArray arrayWithArray:backdropsArray];
   
   if (1 < [backdropsArray count]) {
      [self saveNextImageFromArray:self.arrayOfSerialMiniPosters
          withNotificationOnFinish:kHomeMiniPostersSavedNotification 
                           withTag:(NSUInteger)2222];
   }      
}

// Start downloading posters from TV Home Home movie
- (void)startDownloadingTVHomeMiniPostersFromArray:(NSArray *)backdropsArray{
   self.arrayOfSerialTVMiniPosters = [NSMutableArray arrayWithArray:backdropsArray];
   
   if (1 < [backdropsArray count]) {
      [self saveNextImageFromArray:self.arrayOfSerialTVMiniPosters
          withNotificationOnFinish:kTVHomeMiniPostersSavedNotification 
                           withTag:(NSUInteger)3333];
   }      
}

// Start downloading posters from Movie Home Home movie
- (void)startDownloadingHomePostersFromArray:(NSArray *)backdropsArray {
   self.arrayOfSerialHomePosters = [NSMutableArray arrayWithArray:backdropsArray];
   
   if (1 < [backdropsArray count]) {
      [self saveNextImageFromArray:self.arrayOfSerialHomePosters
          withNotificationOnFinish:kHomePostersSavedNotification 
                           withTag:(NSUInteger)4444];
   }      
}

// CDP backdrops downloader
- (void)startDownloadingBackdropsFromArray:(NSArray *)backdropsArray{
   self.arrayOfSerialBackdrops = [NSMutableArray arrayWithArray:backdropsArray];
   
   if (1 < [backdropsArray count]) {
      [self.arrayOfSerialBackdrops removeObjectAtIndex:0];
      [self saveNextImageFromArray:self.arrayOfSerialBackdrops
          withNotificationOnFinish:kBackdropSavedNotification 
                           withTag:(NSUInteger)5555];
   }   
}

// CDP posters downloader
- (void)startDownloadingPostersFromArray:(NSArray *)postersArray{
   _firstPosterWasDownloaded = NO;
   self.arrayOfSerialPosters = [NSMutableArray arrayWithArray:postersArray];
   [self saveNextImageFromArray:self.arrayOfSerialPosters
       withNotificationOnFinish:kPosterSavedNotification 
                        withTag:(NSUInteger)6666];
}


// Donwloading next miniposter for Home Home movie
- (void)downloadNextMiniHomePosterAfterDelay:(NSNotification *)notification {
   if (0 < [self.arrayOfSerialMiniPosters count]) {
      [self.arrayOfSerialMiniPosters removeObjectAtIndex:0];  
   }   
   [self saveNextImageFromArray:self.arrayOfSerialMiniPosters
       withNotificationOnFinish:kHomeMiniPostersSavedNotification 
                        withTag:(NSUInteger)7777];  
}

// Donwloading next tv miniposter for Home Home movie
- (void)downloadNextTVMiniHomePosterAfterDelay:(NSNotification *)notification {
   if (0 < [self.arrayOfSerialTVMiniPosters count]) {
      [self.arrayOfSerialTVMiniPosters removeObjectAtIndex:0];  
   }
   [self saveNextImageFromArray:self.arrayOfSerialTVMiniPosters
       withNotificationOnFinish:kTVHomeMiniPostersSavedNotification 
                        withTag:(NSUInteger)8888];
}

- (void)downloadNextTVHomePosterAfterDelay:(NSNotification *)notification {
//   [[NSNotificationCenter defaultCenter] postNotificationName:kFinishedDownloadTVHomePostersNotification object:nil];
   if (0 < [self.arrayOfSerialTVHomePosters count]) {
      [self.arrayOfSerialTVHomePosters removeObjectAtIndex:0];  
   }
   [self saveNextImageFromArray:self.arrayOfSerialTVHomePosters
       withNotificationOnFinish:kTVHomePostersSavedNotification 
                        withTag:(NSUInteger)9999];
}

- (void)downloadNextHomePosterAfterDelay:(NSNotification *)notification {
//   [[NSNotificationCenter defaultCenter] postNotificationName:kFinishedDownloadHomePostersNotification object:nil];
   if (0 < [self.arrayOfSerialHomePosters count]) {
      [self.arrayOfSerialHomePosters removeObjectAtIndex:0];  
   }
   [self saveNextImageFromArray:self.arrayOfSerialHomePosters
       withNotificationOnFinish:kHomePostersSavedNotification 
                        withTag:(NSUInteger)2111];
}

- (void)downloadNextBackdropAfterDelay:(NSNotification *)notification{
   if (0 < [self.arrayOfSerialBackdrops count]) {
      [self.arrayOfSerialBackdrops removeObjectAtIndex:0];  
   }
   if (!_backdropsRetrievalPaused && 0 < [self.arrayOfSerialBackdrops count]) {
      [self saveNextImageFromArray:self.arrayOfSerialBackdrops 
          withNotificationOnFinish:kBackdropSavedNotification 
                           withTag:(NSUInteger)3111];
   }  
}

- (void)downloadNextPosterAfterDelay:(NSNotification *)notification{
   if (0 < [self.arrayOfSerialPosters count]) {
      [self.arrayOfSerialPosters removeObjectAtIndex:0];
   }
   if ((_firstPosterImageDownloadedNotification) && (!_firstPosterWasDownloaded)) {
      _firstPosterWasDownloaded = YES;
      [[NSNotificationCenter defaultCenter] postNotificationName:_firstPosterImageDownloadedNotification 
                                                          object:nil];
   }
    if (!_postersRetrievalPaused && 0 < [_arrayOfSerialPosters count]) {
      [self saveNextImageFromArray:self.arrayOfSerialPosters
          withNotificationOnFinish:kPosterSavedNotification 
                           withTag:(NSUInteger)4111];
   } 
}

- (void)pauseDownloadingBackdrops {
   _backdropsRetrievalPaused = YES;
}

- (void)continueDownloadingBackdrops {
   _backdropsRetrievalPaused = NO;
   [self saveNextImageFromArray:self.arrayOfSerialBackdrops
       withNotificationOnFinish:kBackdropSavedNotification 
                        withTag:(NSUInteger)5111];
}

- (void)pauseDownloadingPosters {
   _postersRetrievalPaused = YES;
}

- (void)continueDownloadingPosters {
   _postersRetrievalPaused = NO;
   [self saveNextImageFromArray:self.arrayOfSerialPosters
       withNotificationOnFinish:kPosterSavedNotification 
                        withTag:(NSUInteger)6111];
}
- (void)cancelAllBackdropsRequests{
   [self.arrayOfSerialBackdrops removeAllObjects];
}

- (void)cancelAllPostersRequests{
   [self.arrayOfSerialPosters removeAllObjects];
}


#pragma mark - Image Saved protocol methods

- (void)imageDownloadedForImageSaver:(ANImageSaver *)imageSaver {
    self.runningConnectionsCount--;
    if (0 > self.runningConnectionsCount) {
        self.runningConnectionsCount = 0;
    }
    
    if (0 < [_globalPendingConnections count]) {
        [self startNextPendingConnectionFromArray:_globalPendingConnections];
    } else if (0 < [_highPendingConnections count]) {
        [self startNextPendingConnectionFromArray:_highPendingConnections];
    } else if (0 < [_mediumPendingConnections count]){
        [self startNextPendingConnectionFromArray:_mediumPendingConnections];
    } else if (0 < [_lowPendingConnections count]) {
        [self startNextPendingConnectionFromArray:_lowPendingConnections];
    } else if (0 < [_lowestPendingConnections count]) {
        [self startNextPendingConnectionFromArray:_lowestPendingConnections];
    }
    
    [self postSaveImagesNotification:imageSaver];
    _sendNotification = NO;
    
    NSString * key = [NSString stringWithFormat:@"%d", (int)imageSaver];
    [_activeRunningConnections removeObjectForKey:key];
    [_globalRunningConnections removeObjectForKey:key]; // it does nothing if it doesn't contain it
}

@end

#pragma mark -
#pragma mark Private methods

@implementation ANImageSaverManager (Private)

/*
   Method used to start a new request for the first image in the imagesArray parameter.
   Parameters:
         - imagesArray : the array of images to be downloaded. The method will create a ANImage saver object for the first element in this array.
                         If this array is empty, no image saver is created
         - notificationOnFinish : a NSString representing the notification triggered after the download of the image.
*/
- (void)saveNextImageFromArray:(NSMutableArray *) imagesArray withNotificationOnFinish:(NSString *)notificationOnFinish withTag:(NSUInteger)tag {
   if (0 < [imagesArray count]) {
      NSDictionary * backdrop = [imagesArray objectAtIndex:0];
      NSString * imageUrl = [backdrop objectForKey:kUmageUrlKey];
      NSString * imagePath = [backdrop objectForKey:kUmagePathUrl];
       ANImageSaver * saver = [[ANImageSaver alloc] initWithUrl:imageUrl
                                                       withPath:imagePath
                                                      imageSize:CGSizeMake(0, 0)
                                                       delegate:self
                                                    andUserData:nil];
      saver.tag = tag;
   }
}

@end

