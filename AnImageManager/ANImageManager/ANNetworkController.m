//
//  FHNetworkController.m
//  05Media
//
//  Created by Andrei Neag on 2/22/11.
//  Copyright 2011 05Media. All rights reserved.
//

#import "ANNetworkController.h"
#import "ANImageHelper.h"

@implementation ANNetworkController

@synthesize wasSuccess = _wasSuccess;
@synthesize error = _error;
@synthesize target= _target;
@synthesize userData = _userData;
@synthesize statusCode = _statusCode;
@synthesize connection = _connection;
@synthesize responseData = _responseData;

#pragma mark -
#pragma mark LifeCycle Methods

- (id)initWithUrl:(NSString *)url target:(id)target actionOnFinish:(SEL)action {
	self = [super init];
	
	if (self) {
      self.wasSuccess = NO;
      _statusCode = 0;
      _target = target; // weak reference
      _action = action;
      _userData = nil;
      if (url) {
         // create the actual connection
         NSURL * theUrl = [[NSURL alloc] initWithString:url];
         NSURLRequest * request = [[NSURLRequest alloc] initWithURL:theUrl 
                                                        cachePolicy:NSURLRequestReturnCacheDataElseLoad 
                                                    timeoutInterval:60];
         //NSURLRequest * request = [[NSURLRequest alloc] initWithURL:theUrl];
         self.responseData = [NSMutableData data];
         self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
      }else {
          if ([self.target respondsToSelector:_action]) {
              [self.target performSelector:_action withObject:self];
          }
      }
	}
	return self;
}

// initiating with user data to be sent back to target
- (id)initWithUrl:(NSString *)url target:(id)target actionOnFinish:(SEL)action andUserData:(NSDictionary *)userData{
	self = [self initWithUrl:url target:target actionOnFinish:action];
	
	if (self) {
      self.userData = userData;
	}
	return self;
}

- (void)dealloc {
    self.target = nil;
    self.connection = nil;
    self.responseData = nil;
}

#pragma mark -
#pragma mark NSURLConnection Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.responseData setLength:0];
   
   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
	self.statusCode = [httpResponse statusCode];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
   self.wasSuccess = NO;
   self.error = error;
    if ([self.target respondsToSelector:_action]) {
        [self.target performSelector:_action withObject:self];
    }
    self.connection = nil;
}

//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
//   return nil;
//}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
   self.wasSuccess = YES;
    if ([self.target respondsToSelector:_action]) {
        [self.target performSelector:_action withObject:self];
    }
//    [ANImageHelper performSelector:_action
//                    forRequestor:self.target];
   self.connection = nil;
}

#pragma mark -
#pragma mark Public Methods

// access the downloaded data
- (NSData *)getData {
	return self.responseData;
}

@end
