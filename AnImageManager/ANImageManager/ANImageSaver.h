//
//  ANImageSaver.h
//
//  Created by Andrei Neag on 02/23/11.
//  Copyright 2011. All rights reserved.
//

/*
 * The image saver object will initiate an image download and will save 
 * the received data on the disk.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ANNetworkController;

@protocol ANImageSaverDelegate;

@interface ANImageSaver : NSObject{
    NSString * _pathToSaveTheImage;
    ANNetworkController * _controller;
    NSString * _notificationString;
    BOOL _wasSuccess;
    CGSize _imageSize;
    // holds the number of retries made before
    unsigned _numberOfRetries;
    // to save the image Url for retry
    NSString *_imageUrl;
    // userDataDictionary
    NSDictionary *_userData;
    // tag .. useful for debugging
    NSUInteger _tag;
    UIImage * _image;
}

// if you do not want the image to be resized, pass CGSizeMake(0,0) for imageSize
- (id)initWithUrl:(NSString *)imageUrl withPath:(NSString *)path imageSize:(CGSize)imageSize andDelegate:(id<ANImageSaverDelegate>) delegate;

- (id)initWithUrl:(NSString *)imageUrl withPath:(NSString *)path imageSize:(CGSize)imageSize delegate:(id<ANImageSaverDelegate>) delegate andUserData:(NSDictionary *)userData;
- (id)initWithUrl:(NSString *)imageUrl withPath:(NSString *)path imageSize:(CGSize)imageSize;

- (void)startDownload;

@property (nonatomic, assign) unsigned numberOfRetries;
@property (nonatomic, assign) BOOL wasSuccess;
@property (nonatomic, strong) NSString * notificationString;
@property (nonatomic, strong) NSString * imageUrl;
@property (nonatomic, assign) CGSize imageSize;
@property (nonatomic, strong) ANNetworkController * controller;
@property (nonatomic, strong) NSDictionary * userData;
@property (nonatomic, assign) NSUInteger tag;
@property (nonatomic, strong) NSString * pathToSaveTheImage;
@property (nonatomic, strong) UIImage * image;
@property (nonatomic, assign) id<ANImageSaverDelegate> imageDelegate;

extern NSString * const kUmageUrlKey;
extern NSString * const kUmagePathUrl;

@end

@protocol ANImageSaverDelegate <NSObject>

@required

- (void)imageDownloadedForImageSaver:(ANImageSaver *)imageSaver;

@end
