//
//  ANNetworkController.h
//
//  Created by Andrei Neag on 2/22/11.
//  Copyright 2011 05Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANNetworkController : NSObject {
	NSMutableData *_responseData;
	NSURLConnection * _connection;

	__unsafe_unretained id _target;		//not owned
	SEL _action; 	
	BOOL _wasSuccess;
	NSError * _error;
   // userData dictionary for adidtional info that must be sent back to target
   NSDictionary *_userData;
   
   NSInteger _statusCode;
}

@property (nonatomic, assign) BOOL wasSuccess;
@property (nonatomic, strong) NSError * error;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, strong) NSDictionary * userData;
@property (nonatomic, assign) NSInteger statusCode;
@property (nonatomic, strong) NSURLConnection * connection;
@property (nonatomic, strong) NSMutableData * responseData;

// Public Methods
- (id)initWithUrl:(NSString *)url target:(id)target actionOnFinish:(SEL)action;
- (NSData *)getData;

@end
