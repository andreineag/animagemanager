//
//  FileManagerHelper.h
//  Meditopia
//
//  Created by Andrei Neag on 06/01/14.
//
//

#import <Foundation/Foundation.h>

@interface FileManagerHelper : NSObject

// File Manager Methods
+ (NSString *)createDataCacheFolder;
+ (NSString *)createFolder:(NSString *)subfolderName inFolder:(NSString *)folderPath;
+ (NSString *)createHistoryFolderForContactPhoneNumber:(NSString *)contactPhoneNnumber;
+ (NSString *)createFolder:(NSString *) name inDocumentsSubFolder:(NSString *)subfolder;
// deletes the folder located at the path "path"
+ (void)deleteFolderAtPath:(NSString *)path;
+ (NSString *)createLibraryCacheFolder;
+ (NSString *)createImagesFolder;
+ (NSString *)createLocalCachedImagesFolder;
+ (void)checkCachedTimeforFileAtPath:(NSString *) filePath;

@end
