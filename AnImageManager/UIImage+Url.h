//
//  UIImage+Url.h
//  PBS
//
//  Created by Andrei Neag on 1/20/15.
//  Copyright (c) 2015 Andrei Neag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Url)

+(UIImage *) imageWithUrl:(NSString *) urlString;

@end
