//
//  UIImage+Url.m
//  PBS
//
//  Created by Andrei Neag on 1/20/15.
//  Copyright (c) 2015 Andrei Neag. All rights reserved.
//

#import "UIImage+Url.h"
#import "ANImageHelper.h"
#import "FileManagerHelper.h"

@implementation UIImage (Url)

+(UIImage *) imageWithUrl:(NSString *) urlString {
    NSString * imageName = [ANImageHelper imageNameFromLinkString:urlString];

    UIImage * image = nil;
    NSString * localimagesFolder = [FileManagerHelper createLocalCachedImagesFolder];
    NSString * localPathForImage = [localimagesFolder stringByAppendingPathComponent:imageName];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    [FileManagerHelper checkCachedTimeforFileAtPath:localPathForImage];
    
    if ([fileManager fileExistsAtPath:localPathForImage]) {
        image = [UIImage imageWithContentsOfFile:localPathForImage];
    } else {
        // This needs to run on main thread so that the image object returned will be the correct one.
        // This will cost performance if used multiple times. Please use ANImageView for cells of other places where you need the images to be loaded quickly.
        image =  [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            if (localPathForImage) {
                NSError * error = nil;
                BOOL success = NO;
                NSData * data = UIImagePNGRepresentation(image);
                success = [data writeToFile:localPathForImage options:NSAtomicWrite error:&error];
            }
        });
    }
    
    return image;
}

@end
