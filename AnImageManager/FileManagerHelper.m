//
//  FileManagerHelper.m
//  Meditopia
//
//  Created by Andrei Neag on 06/01/14.
//
//
// Class used to create and manage the folders for local cache or for creating different directories

#import "FileManagerHelper.h"
#import "Defines.h"

@implementation FileManagerHelper

#pragma mark -
#pragma mark File Manager Methods

/*
 Create a folder with the name of the current login user on local memory;
 */
+ (NSString *)createDataCacheFolder {
    NSString * localCacheFolder = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * userName = @"PBS_Image_Cache";
    localCacheFolder = [self createFolder:userName inFolder:localCacheFolder];
    
    return localCacheFolder;
}

+ (NSString *)createFolder:(NSString *)subfolderName inFolder:(NSString *)folderPath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *pathToFolder = folderPath;
    
    // If no folder path was provided default to home
    if (nil == pathToFolder || 0 == [pathToFolder length]) {
        pathToFolder = NSHomeDirectory();
    }
    
    if (nil != subfolderName || 0 != [subfolderName length]) {
        pathToFolder = [pathToFolder stringByAppendingPathComponent:subfolderName];
        if (NO == [fileManager fileExistsAtPath:pathToFolder]) {
            // create the subfolder
            [fileManager createDirectoryAtPath:pathToFolder withIntermediateDirectories:NO attributes:nil error:NULL];
        }
    }
    
    return pathToFolder;
}

+ (NSString *)createHistoryFolderForContactPhoneNumber:(NSString *)contactPhoneNnumber {
    return [FileManagerHelper createFolder:contactPhoneNnumber inFolder:[self createDataCacheFolder]];
}

+ (NSString *)createFolder:(NSString *) name inDocumentsSubFolder:(NSString *)subfolder {
    NSFileManager * fileManager         = [NSFileManager defaultManager];
    NSArray    *paths                   = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString   *pathToDocumentsFolder   = [paths objectAtIndex:0];
    NSMutableString   * pathToFolder           = nil;
    if(nil != subfolder) {
        pathToFolder = [[NSMutableString alloc] initWithFormat:@"%@/%@", pathToDocumentsFolder, subfolder];
        if(NO == [fileManager fileExistsAtPath:pathToFolder]) {
            // create the subfolder
            [fileManager createDirectoryAtPath:pathToFolder withIntermediateDirectories:NO attributes:nil error:NULL];
        }
        [pathToFolder appendFormat:@"/%@", name];
    } else {
        pathToFolder = [NSMutableString stringWithFormat:@"%@/%@", pathToDocumentsFolder, name];
    }
    
    // create the folder
    [fileManager createDirectoryAtPath:pathToFolder withIntermediateDirectories:NO attributes:nil error:NULL];
    
    return pathToFolder;
}

// deletes the folder located at the path "path"
+ (void)deleteFolderAtPath:(NSString *)path {
    NSError * deleteErr = nil;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&deleteErr];
    if(nil != deleteErr) {
        NSLog(@"Error when trying to delete folder: %@", [deleteErr localizedDescription]);
    }
}

+ (NSString *)createLibraryCacheFolder {
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (NSString *)createImagesFolder {
    return [self createFolder:kImagesCacheFolder inFolder:[self createLibraryCacheFolder]];
}

// create a local folder to save the images for all coupons
+ (NSString *)createLocalCachedImagesFolder {
    return [FileManagerHelper createFolder:kLocalCacheFolder inFolder:[FileManagerHelper createImagesFolder]];
}

+ (void)checkCachedTimeforFileAtPath:(NSString *) filePath {
    // See if the image did not expired in the meantime...
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSDictionary* attrs = [fileManager attributesOfItemAtPath:filePath error:nil];
    
    if (attrs != nil) {
        NSDate *cacheDate = (NSDate*)[attrs objectForKey: NSFileCreationDate];
        NSDate *now = [NSDate date];
        NSTimeInterval cacheTimePassed = [now timeIntervalSinceDate:cacheDate];
        if (cacheTimePassed >= kCacheTimeInSeconds) {
            [fileManager removeItemAtPath:filePath error:NULL];
        }
    }
    
}

@end
