# README #

This image download and cache library is intended to ease the loading of images for iPhone developers. The library is intended to be use in applications with a lot of images all through the app. Images are automatically downloaded and stored locally for a given time period. If the image link changes, the library knows to update the image. If any image is reloaded, next time, the image is loaded instantly from the local storage.
The cache images are not stored in memory, they are lazy-loaded when needed.

### What is this repository for? ###

* The library can be used in any project that requires cache for images
* 1.0.2

### How do I get set up? ###

* Download the ANImage demo project from git
* No configuration needed
* Import the "ANImageManager" folder into your application

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact